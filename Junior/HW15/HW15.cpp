﻿#include <iostream>

void EvenUneven(int n, int b)
{
    if (b == 0)
    {
        std::cout << "\nAll uneven numbers from 0 to " << n << ":\n";
        for (int i = 1; i < n; ++i)
        {
            if (i % 2 != 0)
            {
                std::cout << i << "\n";
            }
        }
    }
    else if (b == 1)
    {
        std::cout << "\nAll even numbers from 0 to " << n << ":\n";
        for (int i = 1; i < n; ++i)
        {
            if (i % 2 == 0)
            {
                std::cout << i << "\n";
            }
        }
    }
}

int main()
{
    int n, even;

    std::cout << "Enter n value : ";
    std::cin >> n;
    std::cout << "\nAll even numbers from 0 to " << n << ":\n";

    for (int i = 1; i < n; ++i)
    {
        if (i % 2 == 0)
        {
            std::cout << i << "\n";
        }
    }

    while (true)
    {
        std::cout << "\nNow the function is working\n0 - withdraw uneven\n1 - withdraw even\nType: ";
        std::cin >> even;
        if (even == 1 || even == 0)
        {
            break;
        }
        else
        {
            std::cout << "\nError! Type 1 or 0!\n";
        }
    }

    EvenUneven(n, even);

    std::cin;
    return 0;
}