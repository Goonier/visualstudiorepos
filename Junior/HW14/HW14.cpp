﻿#include <iostream>
#include <string>

int main()
{
    std::cout << "Enter something: ";
    std::string something;
    std::getline(std::cin, something);

    std::cout << something << "\n" << something.length() 
        <<"\n" << something[0] << "\n" << something[something.length()-1];

    std::cin;
    return 0;
}

