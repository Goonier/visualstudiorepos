#include <iostream>
#include <cmath>

using namespace std;

class DynamicArray
{
public:
	DynamicArray()
	{
		n = 3;
		m = 3;

		ArrayBody = CreateDynamicArray(n, m);
		FillDynamicArrayRandomNumbers(n, m);
	}


	explicit DynamicArray(int x, int y)
	{
		n = x;
		m = y;

		ArrayBody = CreateDynamicArray(n,m);
		FillDynamicArrayRandomNumbers(n, m);
	}

	explicit DynamicArray(int x)
	{
		n = x;
		m = x;

		ArrayBody = CreateDynamicArray(n, m);
		FillDynamicArrayRandomNumbers(n, m);
	}

	DynamicArray(DynamicArray& other)
	{
		std::cout << "\nCopy constructor \n";
		n = other.n;
		m = other.m;
		
		if (other.ArrayBody)
		{
			if (ArrayBody) DeleteDynamicArray(ArrayBody, n);
			ArrayBody = CreateDynamicArray(n, m);
			CopyOtherDynamicArrayNumbers(n, m, other);
		}
	}

	DynamicArray& operator=(DynamicArray& other)
	{
		std::cout << "operator = ";
		n = other.n;
		m = other.m;

		if (other.ArrayBody)
		{
			if (ArrayBody) DeleteDynamicArray(ArrayBody, n);
			ArrayBody = CreateDynamicArray(n, m);
			CopyOtherDynamicArrayNumbers(n,m,other);
		}

		if (other.Info)
		{
			if (Info) delete Info;
			Info = new std::string(*(other.Info));
		}

		if (other.Fraction)
		{
			if (Info) delete Fraction;
			Fraction = new float(*(other.Fraction));
		}

		if (other.Word)
		{
			if (Word) delete Word;
			Word = new char(*(other.Word));
		}

		return (*this);
	}

	void ViewDynamicArray()
	{
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < m; j++)
			{
				cout << ArrayBody[i][j] << "\t";
			}
			cout << endl;
		}
	}

private:
	int n, m;
	
	int** ArrayBody;

	std::string* Info;
	float* Fraction;
	char* Word;

	int** CreateDynamicArray(int rows, int cols)
	{
		if (rows <= 0 || cols <=0) {return 0;}

		int **Body = new int* [rows];
		for (int i = 0; i < rows; i++)
		{
			Body[i] = new int[cols];
		}

		return Body;
	}

	void FillDynamicArrayRandomNumbers(int rows, int cols)
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				ArrayBody[i][j] = rand() % 20;
			}
		}
	}

	void CopyOtherDynamicArrayNumbers(int rows, int cols, const DynamicArray &other)
	{
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				ArrayBody[i][j] = other.ArrayBody[i][j];
			}
		}
	}

	void DeleteDynamicArray(int **Body, int rows)
	{
		for (int i = 0; i < rows; i++)
		{
			delete[] Body[i];
		}
		delete[] Body;
	}


};

int main()
{
	DynamicArray v1(3, 4);
	DynamicArray v2(5);

	v1.ViewDynamicArray();
	cout << endl;
	v2.ViewDynamicArray();
	cout << endl;

	DynamicArray v3;
	v3 = v2 = v1;
	cout << endl;

	v3.ViewDynamicArray();
	cout << endl;
	v2.ViewDynamicArray();
}

