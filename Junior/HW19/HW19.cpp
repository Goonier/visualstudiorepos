﻿#include <iostream>
#include <string>

using namespace std;

class Animal
{
public:
    string Voice(string sound)
    {
        return sound + '\n';
    }
};

class Dog : Animal
{
private:
    string a, scream = "Woof!";
public:
    Dog()
    {
      a = Voice(scream);
    }
    
    string GetScram()
    {
        return a;
    }
};

class Frog : Animal
{
private:
    string a, scream = "Kwaa!";
public:
    Frog()
    {
        a = Voice(scream);
    }

    string GetScram()
    {
        return a;
    }
};

class Cat : Animal
{
private:
    string a, scream = "Meow!";
public:
    Cat()
    {
        a = Voice(scream);
    }

    string GetScram()
    {
        return a;
    }
};

class Mouse : Animal
{
private:
    string a, scream = "Squeak!";
public:
    Mouse()
    {
        a = Voice(scream);
    }

    string GetScram()
    {
        return a;
    }
};

class Fox : Animal
{
private:
    string a, scream = "Hehehe!";
public:
    Fox()
    {
        a = Voice(scream);
    }

    string GetScram()
    {
        return a;
    }
};


int main()
{
    Dog* a = new Dog;
    Frog* b = new Frog;
    Cat* c = new Cat;
    Mouse* d = new Mouse;
    Fox* e = new Fox;

    string Animals[5] = {e->GetScram(), b->GetScram(), d->GetScram(), a->GetScram(), c->GetScram()};

    for (string element : Animals)
    {
        cout << element;
    }
}

