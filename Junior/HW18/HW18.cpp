﻿#include <iostream>
#include <string>

using namespace std;

class Stack
{
private:
	int NumArray = 0;
	int* StackArray = new int[NumArray];
public:
	Stack()
	{

	}
	//Добавляет элемент
	void Push(int Value)
	{
		int* NewArray = new int[NumArray + 1];
		cout << "Added item " << Value << endl;

		for (int i = 0; i < NumArray; i++)
		{
			NewArray[i] = StackArray[i];
		}
		NewArray[NumArray] = Value;
		delete[] StackArray;
		NumArray++;
		StackArray = NewArray;
	}
	//Удаляет последний элемент
	void Del()
	{
		cout << "Remove the last item - "<< StackArray[NumArray - 1] << endl;
		int* NewArray = new int[NumArray - 1];
		for (int i = 0; i < NumArray - 1; i++)
		{
			NewArray[i] = StackArray[i];;
		}
		delete[] StackArray;
		NumArray--;
		StackArray = NewArray;
	}

	//Достать верхний элемент из стека
	int Pop()
	{
		return StackArray[NumArray - 1];
	}

	//Печатает массив
	void Print()
	{
		for (int i = 0; i < NumArray; i++)
		{

			cout << *(StackArray + i) << "   ";
		}

		cout << endl;
	}
	//Чистим память
	~Stack()
	{
		delete[] StackArray;
	}

};

int main()
{
	Stack v;
	v.Push(4);
	v.Push(8);
	v.Push(15);
	v.Push(16);
	v.Push(23);
	v.Push(42);
	v.Print();
	cout << v.Pop() << endl;
	v.Del();
	v.Print();
	cout << v.Pop() << endl;
	v.Del();
	v.Print();
	cout << v.Pop() << endl;
	v.Del();
	v.Print();
	cout << v.Pop() << endl;
}
