﻿#include <iostream>
#include <ctime>


int main()
{
    int n,rem,sum=0; // rem - остаток деления.
    int array[100][100];

    struct tm timenow;
    time_t now = time(0);
    localtime_s(&timenow, &now);
    int day = timenow.tm_mday;

    std::cout << "Enter n value : ";
    std::cin >> n;

    rem = day % n;

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            array[i][j] = i + j;
            sum = sum + array[i][j];
            std::cout << array[i][j]<<" ";
        }
        std::cout << "\n";
    }

    std::cout <<"\n"<<day<<" % "<<n<<" = "<<rem<<"\n\nDisplaying sum:\n";

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (i == rem)
            {
                array[i][j] = sum;
            }
            std::cout << array[i][j] << " ";
        }
        std::cout << "\n";
    }
   
    return 0;
}


