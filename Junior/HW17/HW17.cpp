﻿#include <iostream>

class DataValue
{
public:
    DataValue() : x(0), y(0), z(0)
    {}
    DataValue(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    int GetA()
    {
        return a;
    }
    void ShowVector()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z;
    }
    void VectorLength()
    {
        std::cout << '\n' << sqrt((x * x) + (y * y) + (z * z));
    }
     void SetA(int newA)
    {
        a = newA;
    }
private:
    int a;
    double x;
    double y;
    double z;
};

int main()
{
    DataValue temp(10,10,10);
    temp.SetA(10);
    std::cout << temp.GetA();
    temp.ShowVector();
    temp.VectorLength();
}