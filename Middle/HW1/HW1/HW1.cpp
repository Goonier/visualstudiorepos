
#include <iostream>
#include <cmath>

class Vector
{
public:
    Vector()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    //Vector(float num) = delete; - Deny initialization by a single number

    explicit Vector(float num)
    {
        x = num;
        y = num;
        z = num;
    }

    Vector(float x, float y, float z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
        //Info = new std::string("Important!");
    }

    Vector(const Vector& other)
    {
        std::cout << "\nCopy constructor \n";
        x = other.x;
        y = other.y;
        z = other.z;
        //Info = new std::string(*(other.Info));
    }

    ~Vector() //Destructor
    {
        //std::cout << "destructor calling\n";
        //delete Info;
    }

    Vector& operator=(Vector& other)
    {
        std::cout << "operator = ";
        x = other.x;
        y = other.y;
        z = other.z;

        if (other.Info)
        {
            if (Info) delete Info;
            Info = new std::string(*(other.Info));
        }
        
        return (*this);
    }

    operator float()
    {
        return sqrt(x * x + y * y + z * z);
    }

    friend Vector operator+(const Vector& a, const Vector& b);

	//Task 2
    friend Vector operator-(const Vector& a, const Vector& b);

    // Task 1
    friend Vector operator*(const Vector& a, const int& b);

    friend std::ostream& operator<<(std::ostream& out, const Vector& v);

    //Task 3
    friend std::istream& operator>>(std::istream& in, Vector& v);

    friend bool operator>(Vector a, Vector b);

    float operator[](int index)
    {
        switch (index)
        {
        case 0:
            return x;
            break;
        case 1:
            return y;
            break;
        case 2:
            return z;
            break;
        default:
            std::cout << "index error";
            break;
        }
    }

private:
    float x;
    float y;
    float z;

    std::string* Info;
};

Vector operator+(const Vector& a, const Vector& b)
{
    return Vector(a.x + b.x, a.y + b.y, a.z + b.z);
}

//Task 2
Vector operator-(const Vector& a, const Vector& b)
{
	return Vector(a.x - b.x, a.y - b.y, a.z - b.z);
}

// Task 1
Vector operator*(const Vector& a, const int& b)
{
    return Vector(a.x * b, a.y * b, a.z * b);
}

std::ostream& operator<<(std::ostream& out, const Vector& v)
{
    out <<"(" << v.x << ", " << v.y << ", " << v.z <<")";
    return out;
}

//Task 3
std::istream& operator>>(std::istream& in, Vector& v)
{
	in >> v.x >> v.y >> v.z;
	return in;
}

bool operator>(Vector a, Vector b)
{
    // Comparison by length
    float fA, fB;
    fA = static_cast<float>(a);
    fB = static_cast<float>(b);
    return fA > fB;
}

class A
{
public:
    A(int a)
    {
        std::cout << "int constructor\n";
        test = a;
    }
    A(char a) = delete;
private:
    int test;
};

int main()
{
    Vector v1(1);
    std::cout << v1;
    


    ////Task 4
    //Vector v1(0, 1, 2);
    //Vector v2(3, 4, 5);

    //std::cout << "Task 1. v1" << v1 << " * 3 = " << v1 * 3 <<";"<< std::endl;
    //std::cout << "Task 2. v2" << v2 << " - v1" << v1 <<" = " << v2 - v1 << "; " << std::endl;

    //std::cout << "Task 3. Enter Vector. Example: 4 6 8 = (4, 6, 8).\nType v3: ";
    //
    //Vector v3;
    //std::cin >> v3;

    //std::cout <<"\nResult: v3" << v3;
}

